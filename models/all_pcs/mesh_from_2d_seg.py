#!/usr/bin/python3

# TODO use vtk for all operations, remove tvtk dependency

import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from tvtk.api import tvtk, write_data
from skimage import measure
import pyacvd
import vtk

from PIL import Image


def mesh_clustering(PolyData):
    """ reduces number of triangles in vtkPolyData object"""
    num_verts = 5000
    PData = tvtk.to_vtk(PolyData)
    cobj = pyacvd.Clustering(PData)
    # cobj = PyACVD.Clustering(PData)
    cobj.GenClusters(num_verts)  # new number of vertices 5000
    cobj.GenMesh()
    b = cobj.ReturnMesh()

    c = vtk.vtkCleanPolyData()
    c.SetInputData(b)
    c.Update()
    c = c.GetOutput()

    cobj = pyacvd.Clustering(c)
    cobj.GenClusters(num_verts)
    cobj.GenMesh()
    c = cobj.ReturnMesh()

    # fix the normals

    triangleCellNormals = vtk.vtkPolyDataNormals()
    triangleCellNormals.SetInputData(c)
    triangleCellNormals.ComputeCellNormalsOff()
    triangleCellNormals.ComputePointNormalsOn()
    triangleCellNormals.ConsistencyOn()
    triangleCellNormals.AutoOrientNormalsOn()
    triangleCellNormals.Update()

    cleanFilter = vtk.vtkCleanPolyData()
    cleanFilter.SetInputData(triangleCellNormals.GetOutput())
    cleanFilter.Update()

    return cleanFilter.GetOutput()


def rgb2id_array(rgb_array):
    """Return identifier array from rgb array"""
    b = rgb_array[:, :, 2].astype(np.uint64)
    g = 256 * rgb_array[:, :, 1].astype(np.uint64)
    r = 256 * 256 * rgb_array[:, :, 0].astype(np.uint64)
    return r + g + b


def plot_contours(array, level=None):
    plt.imshow(array)
    for id in np.unique(array):
        contours = measure.find_contours(array, id)
        for n, contour in enumerate(contours):
            plt.plot(contour[:, 1], contour[:, 0], linewidth=2, color='black')


def extend_array(array, depth, cid):
    new_array = np.zeros([array.shape[0], array.shape[1], depth])
    bl = np.zeros([array.shape[0], array.shape[1], 1])

    for i in range(depth):
        new_array[np.where(array == cid)] = 1
    new_array = np.concatenate((bl, new_array, bl), axis=2)
    return new_array


def pick_cell(image):
    fig, ax = plt.subplots()
    plt.imshow(image, alpha=0.5)

    def onclick(event):
        global cell_id
        cell_id = image[int(event.ydata), int(event.xdata)]
        print(cell_id)
        fig.canvas.mpl_disconnect(click_id)
        plt.close(1)

    click_id = fig.canvas.mpl_connect('button_press_event', onclick)
    plot_contours(image)
    plt.xlim([0, image.shape[1]])
    plt.ylim([image.shape[0], 0])
    plt.show()
    return cell_id


def marching_cubes_tvtk(array, depth, cid):
    # vx, vy, vz = 11.8, 11.8, 07.9  # in microns
    vx, vy, vz = 1, 1, 1  # in microns

    array = extend_array(array, depth, cid)

    # scikit image marching cubes
    # set degenerate?

    verts, faces, normals, values = measure.marching_cubes_lewiner(array, 0.5)

    # rescale to voxel dimensions (microns)
    for vert in verts:
        vert[0] *= vx
        vert[1] *= vy
        vert[2] *= vz

    for norm in normals:
        norm[0] *= vx
        norm[1] *= vy
        norm[2] *= vz

    # using tvtk to create mesh object

    # polydata = vtk.vtkPolyData()
    # polydata.SetVerts(verts)
    # polydata.SetPolys(faces)
    # polydata.GetPointData.SetNormals(normals)
    # return polydata
    mesh_tvtk = tvtk.PolyData(points=verts, polys=faces)
    mesh_tvtk.point_data.normals = normals
    mesh_tvtk.point_data.normals.name = 'Normals'

    return tvtk.to_vtk(mesh_tvtk), array


def init_from_ply(ply_path):
    import subprocess
    ply_path = os.path.abspath(ply_path)
    conv_path = "/home/ross/home3/tissue/bin/converter"
    init_path = os.path.splitext(ply_path)[0] + ".init"
    command1 = "{} -input_format ply '{}' > '{}'".format(
        conv_path, ply_path, init_path)
    print(command1)
    subprocess.call(command1, shell=True)
    appendpath = "/home/ross/home3/pc_mechanics/scripts/converters/append_cell_data.sh"
    subprocess.call("{} '{}'".format(appendpath, init_path), shell=True)


def tvtk_output_2d(im_path, output_dir, stl_output, cell_id):
    image = Image.open(im_path)
    array = np.asarray(image)
    if im_path.endswith(".png"):
        array = rgb2id_array(array)

    cell_id = int(cell_id)

    depth = 30

    mesh, array = marching_cubes_tvtk(array, depth, cell_id)
    # mesh = mesh_clustering(mesh)
    tvtk_mesh = tvtk.to_tvtk(mesh)

    import tifffile as tf

# fname = 'pavement_cell_' + str(cell_id)
# fname=os.path.splitext(os.path.basename(im_path))[0]

    (head, tail) = os.path.split(im_path)

    (fname, ext) = os.path.splitext(tail)

    fname = fname.replace(" ", "_") + "_{}".format(cell_id)

    if not output_dir:
        out_path = os.path.join(head, fname)
    else:
        out_path = os.path.join(output_dir, fname)

    write_data(tvtk_mesh, out_path + '.vtk')

    writer = vtk.vtkPLYWriter()
    writer.SetInputData(mesh)
    writer.SetFileName(out_path + '.ply')
    writer.Write()

    tf.imsave(out_path + '.tif', array.astype('float32'),
              imagej=True)

    print("saved to : {}".format(out_path))

    init_from_ply(out_path + '.ply')

    return out_path + ".vtk"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('im_path')
    parser.add_argument("-o", "--output_dir", default=None)
    parser.add_argument("-s", "--stl_output", action='store_true')
    parser.add_argument("-c", "--cell_id", default=False)
    args = parser.parse_args()

    tvtk_output_2d(args.im_path, args.output_dir,
                   args.stl_output, args.cell_id)

    # TODO check format of image
    # TODO check style of cell id encoding
