# eng_et_al_2021

Simulation code for the paper "KATANIN and CLASP function at different spatial scales to mediate microtubule response to mechanical stress in Arabidopsis cotyledon" Eng, R. et al, Current Biology, 2021

# Pavement Cell Simulations

The files to complete the simulations for figure 1 can be found in the `models/all_pcs` directory.

The script `mesh_all.py` will create the relevant simulation files from the segmented image `2D_PM_LTIi6b-GFP mChTUA5 WT 48h S1.tifws_seg.png` (These simulation files are provided)

To run the simulations the Makefile will need to be changed. On line 1 add the path to the tissue simulator binary. The tissue modelling software can be obtained [here](https://gitlab.com/slcu/teamHJ/tissue).

Running `make simulations` in the all_pcs directory will run all the simulations.

The python script `project_stresses.py` will create a tiff image of the stresses and strains in the same format as the segmented image to allow for comparison to the original microscopy files.

# Contact
<ross.carter@slcu.cam.ac.uk>

<henrik.jonsson@slcu.cam.ac.uk>
